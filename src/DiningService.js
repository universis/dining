import { ApplicationService, Args } from "@themost/common";
import LocalScopeAccessConfiguration from './config/scope.access.json';
import { StudentReplacer } from './StudentReplacer';
import path from 'path';
const el = require('./locales/el.json');

export class DiningService extends ApplicationService {
    constructor(app) {
        super(app);
        
        // extend Student
        new StudentReplacer(app).apply();

        // extend universis api scope access configuration
        if (app && app.container) {
            app.container.subscribe((container) => {
                if (container) {
                    // add extra scope access elements
                    const scopeAccess = app.getConfiguration().getStrategy(function ScopeAccessConfiguration() { });
                    if (scopeAccess != null) {
                        scopeAccess.elements.push.apply(scopeAccess.elements, LocalScopeAccessConfiguration);
                    }
                    const translateService = app.getService(function TranslateService() {});
                    if (translateService) {
                        translateService.setTranslation('el', el);
                    }
                }
            });
        }
        const mailTemplateService = this.getApplication().getService(function MailTemplateService() { });
        if (mailTemplateService != null) {
            mailTemplateService.extraTemplates.push({
                name: 'mail-after-dining-request-status-change',
                path: path.resolve(__dirname, './templates/mail-after-dining-request-status-change/html.ejs')
            });
            mailTemplateService.extraTemplates.push({
              name: 'mail-after-dining-card-active-status-change',
              path: path.resolve(__dirname, './templates/mail-after-dining-card-active-status-change/html.ejs')
          });
        }
    }

    /**
     * Returns the next request number based on the given request event
     * @param {*} context 
     * @param {*} requestEvent 
     * @returns 
     */
    async nextNumber(context, requestEvent) {
        // validate id
        const model = context.model('DiningRequestEvent');
        const parentRequestEvent = model.convert(requestEvent);
        const id = parentRequestEvent.id;
        Args.notNumber(id, 'Item identifier');
        let lastIndex;
        await new Promise((resolve, reject) => {
          context.db.executeInTransaction(cb => {
            // execute async method
            (async function () {
              /**
             * @type {{lastIndex: any}}
             */
              const item = await model.where('id').equal(id).select('lastIndex').silent().getItem();
              //get last index or 0
              lastIndex = item.lastIndex ? item.lastIndex : 0;
              // increase last index
              lastIndex += 1;
              // save last index
              await model.silent().save({ id, lastIndex });
            })().then(() => {
              return cb();
            }).catch(err => {
              return cb(err);
            });
          }, err => {
            if (err) {
              return reject(err);
            }
            return resolve();
          });
        });
        return lastIndex;
      }

}
