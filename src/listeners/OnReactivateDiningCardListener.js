import { DataPermissionEventListener, PermissionMask } from "@themost/data";
import { DataError } from '@themost/common';
import { promisify } from "util";

async function beforeSaveAsync(event) {
    //
    if (event.state !== 2) {
        return;
    }

    const context = event.model.context;

    const previous = event.previous;
    if (previous == null) {
        throw new DataError('E_PREVIOUS', 'The previous state of the object cannot be determined');
    }

    const target = event.target;

    if (target.active === false && previous.active === true) {
        // get validator listener
        const validator = new DataPermissionEventListener();
        // noinspection JSUnresolvedFunction
        const validateAsync = promisify(validator.validate)
            .bind(validator);
        // validate StudentDiningCard/Cancel execute permission
        const validateEvent = {
            model: context.model('StudentDiningCard'),
            privilege: 'StudentDiningCard/Cancel',
            mask: PermissionMask.Execute,
            target: event.target && event.target.id,
            throwError: true
        }
        await validateAsync(validateEvent);

    }
}

async function afterSaveAsync(event) {
    if (event.state !== 2) {
        return;
    }

    const context = event.model.context;

    const previous = event.previous;
    if (previous == null) {
        throw new DataError('E_PREVIOUS', 'The previous state of the object cannot be determined');
    }

    const target = event.target;

    if (target.active === false && previous.active === true) {
        //save 
        await context.model('CancelDiningCardAction').save({
            diningCard: target.id,
            actionStatus: {
                alternateName: 'CompletedActionStatus'
            }
        })
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}