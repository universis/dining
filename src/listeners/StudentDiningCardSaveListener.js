/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
import {DataError} from "@themost/common";
import * as CancelDiningCardListener from './OnCancelDiningCardListener';

export function beforeSave(event, callback) {
    (async () => {

        // validate student state (on insert or update)
        if (event.state === 1 || event.state === 2) {
            let context = event.model.context;
            /**
             * @type {Student|DataObject|*}
             */
            const student = context.model('Student').convert(event.target.student);
            const isActive = await student.silent().isActive();
            // allow card to be set as active=false in any case of student
            if (event.state === 2 && !isActive && event.previous.active != event.target.active && event.previous.active === false) {
                throw new DataError('ERR_STATUS',
                    'Invalid student status. Student Dining Card may be published upon an active student only.' + student
                    , null,
                    'Student', 'studentStatus');
            }



            if (event.state === 2 && event.previous.active === false && event.target.active === true) {
                
                // evoked when event started form dining card re-activation
                const validThrough = await context.model('StudentDiningCard')
                .where ('id')
                .equal(event.target.id)
                .select('action/diningRequestEvent/validThrough as validThrough')
                .silent()
                .value();

                event.target.validThrough = validThrough;
                event.target.cancelReason = null;
                event.target.dateCancelled = null;
            }



        }
    })().then( () => {
       return callback();
    }).catch( err => {
        return callback(err);
    });
}


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    (async () => {
        let context = event.model.context;

 /**
 * @type {import("@themost/data").DataModel}
 */

 const translateService = context.application.getStrategy(function TranslateService() { });


        // remove listener because the following cards
        // if exist, should be cancelled 
        const StudentDiningCard = context.model('StudentDiningCard');
        StudentDiningCard.removeListener('before.save', CancelDiningCardListener.beforeSave)
        StudentDiningCard.removeListener('after.save', CancelDiningCardListener.afterSave)
        

        // search for diningCards that may be active (true)
        // in other dining request events that are not opened and cancel them

        if ( event.target.active === true && event.state === 1 ) {
            let cardsToCancel = await context.model('StudentDiningCards')
                .where('student')
                .equal(event.target.student.id || event.target.student)
                .and('active')
                .equal(true)
                .and('id')
                .notEqual(event.target.id)
                .and('action/diningRequestEvent/eventStatus/alternateName')
                .notEqual('EventOpened')
                .expand({
                    name: 'action',
                    options: {
                        $expand: 'diningRequestEvent($expand=eventStatus)'
                    }
                })
                .silent()

                .getItems();

                if (Array.isArray(cardsToCancel) && cardsToCancel.length > 0 ) {
                    const cancelReason = translateService.translate('StudentDiningCardSystemCancelReasonNewWasIssued');
                    
                    cardsToCancel = cardsToCancel.map(el => {el.active = false;
                        el.validThrough = new Date();
                        el.cancelReason  = cancelReason;
                        return el;
                    });
                        await context.model('StudentDiningCard').silent().save(cardsToCancel);
            
                }
        }


    })().then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
