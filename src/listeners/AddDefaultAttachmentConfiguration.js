import {DataError} from '@themost/common';
import {DataObjectState} from '@themost/data';
const DefaultAttachmentConfigurations = require('./DefaultAttachmentConfigurations.json');
const DefaultAttachmentTypes = require('./DefaultAttachmentTypes.json');
// eslint-disable-next-line no-unused-vars
async function beforeSaveAsync(event) {
    if (event.state !== DataObjectState.Insert) {
        return;
    }
    const context = event.model.context;
    if (event.target.attachmentTypes == null) {
        // force add attachment types even if they are already defined
        await context.model('AttachmentType').silent().save(DefaultAttachmentTypes);
        event.target.attachmentTypes = DefaultAttachmentConfigurations;
    }
}

async function afterSaveAsync(event) {
    //
} 

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}