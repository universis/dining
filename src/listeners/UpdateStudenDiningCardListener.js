import { DataError } from '@themost/common';
import * as CancelDiningCardListener from './OnCancelDiningCardListener';

async function afterSaveAsync(event) {
    //
    if (event.state !== 2) {
        return;
    }

    const context = event.model.context;


    if (!(event.target.hasOwnProperty('studentStatus') && event.target.studentStatus != null)) {
        return;
    }

    let targetStudentStatus = await context.model('StudentStatus').find(event.target.studentStatus).getItem();

        if (targetStudentStatus == null) {
            return;
        }


    const translateService = context.application.getStrategy(function TranslateService() { });

    const previousStudentStatus = event.previous && event.previous.studentStatus;
    if (previousStudentStatus == null) {
        throw new DataError('E_PREVIOUS', 'The previous student status cannot be determined');
    }

    // students on active or declared actionStatus keep their StudentDiningCard on active state
    if (targetStudentStatus &&
        ((previousStudentStatus.alternateName === 'active' && (targetStudentStatus.alternateName != 'active' && targetStudentStatus.alternateName != 'declared'))
            ||
            (previousStudentStatus.alternateName === 'declared' && (targetStudentStatus.alternateName != 'declared' && targetStudentStatus.alternateName != 'active')))) {

        // find StudentDiningCard 
        const studentDiningCard = await context.model('StudentDiningCards')
            .where('student')
            .equal(event.target.id)
            .and('active')
            .equal(true)
            .select('id', 'active', 'student')
            .flatten()
            .silent()
            .getItem();

        if (typeof studentDiningCard != 'undefined' && studentDiningCard != null) {

            const cancelReason = translateService.translate('StudentDiningCardSystemCancelReason');
            let actionDateNote;
            //dateCancelled value ONLY for the "cancelReason" info field 
            let dateCancelled;

            studentDiningCard.active = false;
            // StudentDiningCard's valid through date is the date when the event (student-suspended, student-erased, student-graduated) is taking place
            studentDiningCard.validThrough = event.target.dateModified;
            // StudentDiningCard's date cancelled is the date when the event (student-suspended, student-erased, student-graduated) is taking place
            studentDiningCard.dateCancelled = event.target.dateModified;

            // set dateCancelled value in relation to target studentStatus  
            switch (targetStudentStatus.alternateName) {
                case "erased":
                    if (typeof event.target.removalDate !== 'undefined' && event.target.removalDate != null) {
                        dateCancelled = event.target.removalDate;
                        actionDateNote = translateService.translate('RemovalDate');
                    }
                    break;
                case "graduated":
                    if (typeof event.target.graduationDate !== 'undefined' && event.target.graduationDate != null) {
                        dateCancelled = event.target.graduationDate;
                        actionDateNote = translateService.translate('GraduationDate');
                    }
                    break;
                case "suspended":
                    // eslint-disable-next-line no-case-declarations
                    const suspensions = await context.model('StudentSuspension')
                        .where('student')
                        .equal(event.target.id)
                        .and('reintegrated')
                        .equal(false)
                        .orderByDescending('suspensionDate')
                        .flatten()
                        .silent()
                        .getItems();
                    if (Array.isArray(suspensions)) {
                        dateCancelled = suspensions[0].suspensionDate;
                        actionDateNote = translateService.translate('SuspensionDate');
                    } else {
                        throw new DataError('E_STUDENT_DINING_CARD', 'Invalid student\'s suspension data');
                    }
                    break;
            }

            // dateCancelled
            studentDiningCard.cancelReason = cancelReason + ' ' + translateService.translate(targetStudentStatus.alternateName) + ' ' + actionDateNote + ' ' + dateCancelled.toLocaleDateString(context.locale);

            /**
             * @type {import("@themost/data").DataModel}
             */
            const StudentDiningCard = context.model('StudentDiningCard');
            StudentDiningCard.removeListener('before.save', CancelDiningCardListener.beforeSave)
            StudentDiningCard.removeListener('after.save', CancelDiningCardListener.afterSave)
            await StudentDiningCard.silent().save(studentDiningCard);
        }
    }

}

async function beforeSaveAsync(event) {
    //
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}