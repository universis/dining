import { DataError } from '@themost/common';

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
	return OnChangeDiningRequestEventListener.afterSaveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}

export function beforeSave(event, callback) {
	return OnChangeDiningRequestEventListener.beforeSaveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}

class OnChangeDiningRequestEventListener {

	static async beforeSaveAsync(event) {
		/**
		 * @type {DataContext|*}
		 */

		const context = event.model.context;
		const target = event.target;

        const translateService = context.application.getStrategy(function TranslateService() { });
		const events = await context.model('DiningRequestEvent')
		.silent().getItems();

		if (event.state === 1) {
			if (target.eventStatus && target.eventStatus.alternateName && (target.eventStatus.alternateName === 'EventOpened' || target.eventStatus.alternateName === 'EventCancelled'))
				if (!target.validThrough || !target.validFrom) {
					throw new DataError('E_EVENT_DATES', translateService.translate('Dining period cannot be created without dates.'), null, 'DiningRequestEvent');
				}

				const exists = events.find(el => el.academicYear && el.academicYear.id && (el.academicYear.id === target.academicYear.id));

				if (exists) {
					throw new DataError('E_EVENT', translateService.translate('Dining period for this academic year already exists.'), null, 'DiningRequestEvent');
				}

			if (target.eventStatus && target.eventStatus.alternateName === 'EventOpened') {
				if (events && Array.isArray(events) && events.length > 0
					&& events.find(el => el.eventStatus && el.eventStatus.alternateName && el.eventStatus.alternateName === 'EventOpened')) {
					throw new DataError('E_EVENT_STATUS', translateService.translate('Event cannot be created in EventOpened status while there is another Event Opened.'), null, 'DiningRequestEvent');
				}
			}

			
			if (Object.prototype.hasOwnProperty.call(target, 'hasConsent') === false) {
				throw new DataError('E_EVENT_HAS_CONSENT', translateService.translate('The event cannot be created without setting the mandatory consent period attribute for the privacy policy.'), null, 'DiningRequestEvent');
			}

		}

		if (event.state === 2) {

			const previous = await context.model('DiningRequestEvent').where('id').equal(target.id).silent().getTypedItem();
			if (!previous) {
				throw new DataError('E_EVENT', translateService.translate('The specified DiningRequestEvent cannot be found or is inaccessible'), null, 'DiningRequestEvent');
			}

			if (previous.eventStatus == null) {
				throw new DataError('E_STATE', translateService.translate('The previous state of the object cannot be determined.'), null, 'DiningRequestEvent');
			}


			if (previous && previous.eventStatus && previous.eventStatus.alternateName === 'EventCancelled' 
				&& target.eventStatus && target.eventStatus.alternateName === 'EventOpened') {
				if (events && Array.isArray(events) && events.length > 0
					&& events.find(el => el.eventStatus && el.eventStatus.alternateName && el.eventStatus.alternateName === 'EventOpened' && el.id !== target.id)) {
					throw new DataError('E_EVENT_STATUS', translateService.translate('Only one dining event can be open.'), null, 'DiningRequestEvent');
				}
			}

			// check if user tries to change hasConsent attribute
			if (previous.hasConsent != null && target.hasConsent != null && (previous.hasConsent != target.hasConsent)) {
				throw new DataError('E_EVENT_HAS_CONSENT', translateService.translate('The privacy policy mandatory consent period attribute cannot be changed.'), null, 'DiningRequestEvent'); // Closed status: to maintain consistency with the user 
			}

			const today = new Date().toISOString();
			if (previous && previous.validFrom != previous.validThrough && target.validThrough
				&& previous.eventStatus && previous.eventStatus.alternateName === 'EventOpened') {
					if (today > previous.validThrough.toISOString() || 
						today > target.validThrough.toISOString()) {
						throw new DataError('E_EVENT_VALID_THROUGH_DATE', 'Valid through date cannot be earlier than today. Dining request event is already expired in the past.', null, 'DiningRequestEvent_ValidThrough');
					}
			}
		}
	}

	static async afterSaveAsync(event) {
     //
	}
}
